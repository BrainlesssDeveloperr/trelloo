<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->timestamp('reminders')->nullable();
            $table->string('status')->nullable();
            $table->string('color')->nullable();
            $table->unsignedBigInteger('list_id');
            $table->timestamps();

            $table->foreign('list_id')
            ->references('id')
            ->on('lists')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
