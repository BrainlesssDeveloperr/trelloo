<div id="create-team" class="d-none mt-5 py-3">
    <div class="wrapper mb-4">
        <div id="close-create-team">X</div>
        <div class="content-wrapper row">
            <div class="create col-md-6">
                <div class="create-wrapper">
                    <h4 class="dark-text">Lets Build a Team</h4>
                    <h5>Boost your productivity by making it easier for everyone to access boards in one location.</h5>
                    <span class="label dark-text">Team Name</span>
                    <input type="text" name="team_name" id="team-name" class="my-input p-2">
                    <span class="light-text info pt-1">This is the name of your company, team or organization.</span>
                    <span class="label dark-text">Team type</span>
                    <select name="team_type" id="team-type" class="my-input p-2">
                        @foreach ($types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                        @endforeach
                    </select>
                    <span class="label dark-text">Description <span class="optional-text">Optional</span></span>
                    <textarea name="team_description" id="team-description" cols="30" rows="5" class="my-input p-2"></textarea>
                    <span class="light-text pb-3 pt-1">This is the name of your company, team or organization.</span>
                    <button class="my-submit-btn disabled-btn" id="team-submit" disabled>Continue</button>
                </div>
            </div>
            <div class="image-section col-md-6 text-center">
                <img src="https://a.trellocdn.com/prgb/dist/images/organization/empty-board.286f8fc83e01c93ed27e.svg" alt="">
            </div>
        </div>
    </div>
</div>

<div id="add-members" class="d-none mt-5 pt-5">
    <div class="wrapper">
        <div id="close-member-modal" class="close-btn">X</div>
        <div class="content-wrapper row">
            <div class="create col-md-6">
                <div class="create-wrapper">
                    <input type="hidden" name="team_id" id="add-member-to-team">
                    <h4 class="dark-text">Invite Your Team</h4>
                    <h5 class="light-text">Trello makes your teamwork best work. Invite your new team members to get going!</h5>

                    <span class="label dark-text">Team Members</span>
                    <input type="text" name="members" id="members" class="my-input p-2">
                    <span class="light-text info pt-1"> <strong>Pro Tip!</strong> Invite all members comma saperated</span>

                    <button class="member-submit-btn disabled-btn" id="member-submit" type="submit" disabled>Continue</button>

                    <div class="inline-block text-center mt-2" id="close-member-modal-btn">I'll do this later</div>
                </div>
            </div>
            <div class="image-section col-md-6 text-center">
                <img src="https://a.trellocdn.com/prgb/dist/images/organization/empty-board.286f8fc83e01c93ed27e.svg" alt="">
            </div>
        </div>
    </div>
</div>
