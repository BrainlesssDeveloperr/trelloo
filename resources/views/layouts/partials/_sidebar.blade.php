
    <div class="sidebar">
        <ul>
            <li class="active"><i class="fa fa-trello" aria-hidden="true"></i> Boards</li>
            <li><i class="fa fa-trello" aria-hidden="true"></i> Templates</li>
            <a href="{{route('home')}}"><li><i class="fa fa-home" aria-hidden="true"></i>Home</li></a>
        </ul>
        <div class="teams">
            <p>TEAMS</p>
            <i class="fa fa-plus" aria-hidden="true" id="create-team-btn"></i>
        </div>
        <ul>
            @foreach ($teams as $team)
                <a href="{{route('showTeam',[$team->id,"null"])}}">
                    <li data-toggle="collapse" href="#collapseTeam" role="button" aria-expanded="false" aria-controls="collapseTeam">
                        <i class="fa fa-users" aria-hidden="true"></i> {{$team->name}}
                    </li>
                </a>
            @endforeach
        </ul>
    </div>
