<section id="header">
    <div class="wrapper">
        <div>
            <a href="{{route('home')}}" class="home element-alignments">
                <i class="fa fa-home" aria-hidden="true"></i>
            </a>
            <div class="boards element-alignments" id="board-btn">
                <i class="fa fa-trello" aria-hidden="true"></i> Boards
            </div>
            <div class="search element-alignments">
                <input type="text" class="search-input"> <i class="fa fa-search" aria-hidden="true"></i>
            </div>
        </div>
        <div>
            <div class="icon">
                <i class="fa fa-trello" aria-hidden="true"></i> Trello
            </div>
        </div>
        <div>
            <div class="create-btn element-alignments">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </div>
            <div class="notification element-alignments" id="notification-btn">
                <i class="fa fa-bell bell" aria-hidden="true"></i>
            </div>
            <div class="logout-btn profile element-alignments">
                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div id="boards-search" class="d-none">
        <div class="close-div pull-right">
            X
        </div>
        <div class="clearfix"></div>
        <div class="boards">
            <ul>
                <li class="board-heading mt-2">
                    <a data-toggle="collapse" href="#collapsePersonal" role="button" aria-expanded="true" aria-controls="collapsePersonal" class="boards-header">
                        <i class="fa fa-trello" aria-hidden="true"></i> personal Boards
                    </a>
                </li>
                <div class="" id="collapsePersonal">
                    <ul>
                        @foreach ($personalBoards as $board)
                            <li class="boards-list">
                                <a href="{{route('boards.boardIndex',$board->id)}}"><span class="white-space">&ThickSpace;&ThickSpace;&ThickSpace;&ThickSpace;</span> <span class="ml-2"></span>{{$board->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{route('boards.index')}}">Show All</a>
                </div>
            </ul>
        </div>
    </div>

    <div id="create-block" class="d-none">
        <div class="text-center header mt-1">
            <div class="close-create-div pull-right p-1 mr-2">
                X
            </div>
            <h6>Create</h6>
        </div>
        <hr class="hr-primary">
        <div class="create-type" data-toggle="modal" data-target="#exampleModal">
            <h5><i class="fa fa-trello" id="create-board-option" aria-hidden="true"></i> Create Board</h5>
            <div class="light-fonts">
                <p class="d-inline-block">A board is made up of cards ordered on lists. Use it to manage projects, track information, or organize anything.</p>
            </div>
        </div>
        <div class="create-type mt-1"  id="create-team-option">
            <h5><i class="fa fa-users" aria-hidden="true"></i> Create Team</h5>
            <div class="light-fonts">
                <p class="d-inline-block">A board is made up of cards ordered on lists. Use it to manage projects, track information, or organize anything.</p>
            </div>
        </div>
    </div>
    <div id="notification-panel" class="d-none">
        <div class="wrapper">
            <div class="content" id="notification-cards">
                <div class="notification-header text-center">
                    <p>Notifications</p>
                    <div id="close-notification-modal" class="close-btn">X</div>
                </div>
                <hr>

                <div id="notification-list">
                    @foreach ($notifications as $notification)
                        <div class="container mt-3" id="notification-card_{{$notification->card_id}}">
                            <div class="row">
                                <div class="col-md-12 p-2">
                                    <div class="bg p-2">
                                        <a href="{{route('boards.boardIndex',$notification->card->list->board)}}">
                                            <div class="pl-1 list">
                                                <p>{{$notification->card->name}}</p>
                                                <div class="d-flex  justify-content-between" id="notification-due-date">
                                                    @if ($notification->card->due_date)
                                                        <p id="due-content" class="d-inline-block bg-danger mb-2 p-1 px-2 text-white">
                                                            <i class='fa fa-clock-o'></i>&MediumSpace;{{date('d-m', strtotime($notification->card->due_date))}}
                                                        </p>
                                                    @else
                                                        <div id="blank-div"></div>
                                                    @endif
                                                    <div class="avatar p-1 text-center mb-2">
                                                        @foreach ($notification->card->users as $user)
                                                            <img src="{{$user->avatar}}" alt="">
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <span class="text-white">{{$notification->card->list->board->name}}: {{$notification->card->list->name}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row px-2">
                                <div class="col-md-12 card-content card-action">
                                    <div class="p-3">
                                        <div class="d-flex" id="sender-card-user" data-user-id="1">
                                            <div class="avatar text-center">
                                                <img src="{{$notification->fromUser->avatar}}" alt="">
                                            </div>
                                            <strong><p class="ml-1">{{$notification->fromUser->name}}</p></strong>
                                        </div>
                                        <div id="notification-actions_{{$notification->card_id}}" data-card-id="{{$notification->card_id}}">
                                            @foreach ($notification->messages as $message)
                                                @if ($message->type == "add-user")
                                                    <a href="{{route('boards.boardIndex',$notification->card->list->board)}}"><p class="d-block"><i class="fa fa-chevron-right pr-1" aria-hidden="true"></i>Added You</p></a>
                                                @elseIf($message->type == "add-checklist")
                                                    <a href="{{route('boards.boardIndex',$notification->card->list->board)}}"><p class="d-block"><i class="fa fa-chevron-right pr-1" aria-hidden="true"></i>Added Checklist</p></a>
                                                @elseIf($message->type == "due-date")
                                                    <a href="{{route('boards.boardIndex',$notification->card->list->board)}}"><p class="d-block"><i class="fa fa-clock-o pr-1 pr-1"></i>Added due date Due 2 Aug at 22:59</p></a>
                                                @elseIf($message->type == "remove-user")
                                                    <a href="{{route('boards.boardIndex',$notification->card->list->board)}}"><p class="d-block"><i class="fa fa-chevron-right pr-1" aria-hidden="true"></i>Removed You</p></a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('boards.store') }}" method="POST">
                        @csrf
                        <div>
                    <div class="row">
                        <div class="col-md-6 ml-2 m-0 bg-color" style="background-color: blue">
                            <div class="pt-2 form-group m-0">
                                <div class="d-flex justify-content-between">
                                    <input type="hidden" name="color_code" id="color_code" value="blue">
                                    <input type="text" name="name" id="name" placeholder="Add Board title" class="input bg-none form-control">
                                    <button type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="text-white" style="font-size: 16px">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group col-md-6 p-0 m-0 mb-1">
                                <select name="team_id" id="team_id" class="bg-none input form-control">
                                    <option class="text-black" value="personal">Personal</option>
                                    @foreach ($teams as $team)
                                        <option class="text-black" value="{{$team->id}}">{{$team->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-0  form-group col-md-6  m-0 mb-1">
                                <select name="visibility" id="visibility" class="bg-none input form-control">
                                    <option class="text-black" value="private"><i class="fa fa-lock"></i>Private</option>
                                    <option class="text-black" value="public"><i class="fa fa-lock"></i>Public</option>
                                </select>
                            </div>
                        </div>
                        <form action="" method="POST">
                            @csrf
                            <div class="col-md-3 m-0 colors form-group" >
                                <div class="row pl-2">
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="blue"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="red"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="brown"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="green"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="pink"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="black"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="orange"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="purple"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div id="color"  class="grey"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <div class="form-group m-0">
                    <button type="submit" class=" btn btn-success form-control" style="width: 100px">Create board</button>
                </div>
            </div>
            </form>

        </div>
    </div>
{{-- END MODAL --}}
</section>
