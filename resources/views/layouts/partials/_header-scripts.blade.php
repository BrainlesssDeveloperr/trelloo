<script>
    $('#close-notification-modal').click(function(e){
        $('#notification-panel').toggleClass('d-block').toggleClass('d-none');
    });
    $('#notification-btn').click(function(e){
        $('#notification-panel').toggleClass('d-block').toggleClass('d-none');
    });
    $('#notification-panel').click(function(e){
        $('#notification-panel').toggleClass('d-block').toggleClass('d-none');
        $('#notification-btn').removeClass('new-notification');
    });
    $('#notification-panel .wrapper').click(function(e){
        e.stopPropagation();
    });

    // add team modal
    $("#team-name").keyup(function(){
        var value =$("#team-name").val();
        value = $.trim(value);
        if(value == ""){
            $(".my-submit-btn").attr("disabled",true);
            $(".my-submit-btn").addClass("disabled-btn");
        }else{
            $(".my-submit-btn").removeAttr("disabled",true);
            $(".my-submit-btn").removeClass("disabled-btn");
        }
    });

    //add member modal
    $("#members").keyup(function(){
        var value =$("#members").val();
        value = $.trim(value);
        if(value == ""){
            $(".member-submit-btn").attr("disabled",true);
            $(".member-submit-btn").addClass("disabled-btn");
        }else{
            $(".member-submit-btn").removeAttr("disabled",true);
            $(".member-submit-btn").removeClass("disabled-btn");
        }
    });

    $("#team-submit").click(function(e){
        var route = "{{ route('team.store') }}";
        var team = $("#team-name").val();
        var description = $('#team-description').val();
        var type = $( "#team-type option:selected" ).text();
        $('#team-name').val("");
        $('#team-description').val("");

        $.ajax({
            url: route,
            method: "POST",
            data:{
                _token: "{{ csrf_token() }}",
                'team_name':team,
                'team_type':type,
                'team_description':description,
            },
            dataType: 'json',
            success: function(success){
                $('#add-member-to-team').val(success);
                $('#create-team').toggleClass('d-none').toggleClass('d-block');
                $('#add-members').toggleClass('d-none').toggleClass('d-block');
            }
        });
    });

    $('#member-submit').click(function (e) {
        var members = $("#members").val();
        var team_id = $('#add-member-to-team').val();
        var route = "{{ route('ajax.addMembers') }}";
        $("#members").val("");
        $.ajax({
            url:route,
            method:"POST",
            data:{
                _token: "{{ csrf_token() }}",
                'members': members,
                'team_id': team_id
            },
            dataType: 'json',
            success:function(success){
                $('#add-members').toggleClass('d-none').toggleClass('d-block')
            }
        })
    });


    $("#create-team-option").click(function(){
        $('#create-block').toggleClass('d-block').toggleClass('d-none');
        $('#create-team').toggleClass('d-block').toggleClass('d-none');
    });
    $("#create-team-btn").click(function(){
        $('#create-team').toggleClass('d-block').toggleClass('d-none');
    });
    $("#close-create-team").click(function(){
        $('#create-team').toggleClass('d-block').toggleClass('d-none');
    });


    $('#create-team .wrapper').click(function(e){
        e.stopPropagation();
    });
    $('#create-team').click(function(){
        $('#create-team').toggleClass('d-block').toggleClass('d-none')
    });

    $("#close-member-modal").click(function(){
        $('#add-members').toggleClass('d-block').toggleClass('d-none');
    });
    $("#close-member-modal-btn").click(function(){
        $('#add-members').toggleClass('d-block').toggleClass('d-none');
    });
    $('#add-members .wrapper').click(function(e){
        e.stopPropagation();
    });
    $('#add-members').click(function(){
        $('#add-members').toggleClass('d-block').toggleClass('d-none');
    });
</script>
