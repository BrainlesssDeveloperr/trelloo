@auth
<script>
    var user_id = {{auth()->user()->id}};
    Echo.private(`notify.${user_id}`)
    .listen('NotificationEvent', (e) => {
        if($('#notification-panel').hasClass('d-none')){
            $('#notification-btn').addClass('new-notification');
        }
        var id = '#notification-actions_'+e.message[3].id;
        var card_id = $(id).data('card-id');
        console.log(e);
        if(card_id == e.message[3].id){
            addToNotification(e,id,card_id);
        }else{
            addNewNotification(e);
        }
    });
    function addNewNotification(e){
        // console.log("new card notification");
        var imgs = "";
        for(var i=0;i<e.message[8];i++){
            imgs += `<img src="${e.message[8][i]}" alt="">`;
        }
        var sender = `<img src="${e.message[9]}" alt="">`;

        $('#notification-list').prepend(`
            <div class="container mt-3" id="notification-card_${e.message[3].id}"">
                <div class="row">
                    <div class="col-md-12 p-2">
                        <div class="bg p-2">
                            <a href=""><div class="pl-1 list">
                                <p>${e.message[3].name}</p>
                                <div class="d-flex  justify-content-between" id="notification-due-date">
                                    <div id="blank-div"></div>
                                    <div class="avatar p-1 text-center">
                                        ${img}
                                    </div>
                                </div>
                            </a>
                            </div>
                            <span class="text-white">${e.message[5].name}: ${e.message[4].name}</span>
                        </div>
                    </div>
                </div>
                <div class="row px-2">
                    <div class="col-md-12 card-content card-action">
                        <div class="p-3">
                            <div class="d-flex" id="sender-card-user" data-user-id="${e.message[6].id}">
                                <div class="avatar text-center">
                                    ${sender}
                                </div>
                                <strong><p class="ml-1">${e.message[6].name}</p></strong>
                            </div>
                            <div id="notification-actions_${e.message[3].id}" data-card-id="${e.message[3].id}">
                                <a href=""><p class="d-block"><i class="fa fa-clock-o pr-1"></i>Added you</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `);
    }
    function addToNotification(e,list,card_id){
        // console.log("add to notification");
        if(e.message[2] == "add-user"){
            // console.log(e.message[2]);
            $(list).append(`<a href=""><p class="d-block"><i class="fa fa-chevron-right pr-1" aria-hidden="true"></i>Added You</p></a>`);
        }else if(e.message[2] == "add-checklist"){
            console.log(e.message[2]);
            $(list).append(`<a href=""><p class="d-block"><i class="fa fa-chevron-right pr-1"></i>Added checklist</p></a>`);
        }
        else if(e.message[2] == "remove-user"){
            $(list).append(`<a href=""><p class="d-block"><i class="fa fa-chevron-right pr-1"></i>Removed you</p></a>`);
        }
        else if(e.message[2] == "due-date"){
            $('#due-content').remove();
            $('#blank-div').remove();
            $('#notification-due-date').prepend(`<p class="d-inline-block bg-danger mb-2 p-1 px-2 text-white" id="due-content"><i class='fa fa-clock-o'></i>  ${e.message[7]}</p>`);
            $(list).append(`<a href=""><p class="d-block"><i class="fa fa-clock-o pr-1"></i>Added due date ${e.message[7]}</p></a>`);
        }

        var id = '#notification-card_'+card_id;
        var card = $(id).html();
        $('#notification-card_'+card_id).remove();
        card = `<div class="container mt-3" id="notification-card_${e.message[3].id}"">`+card+"</div>";

        $('#notification-list').prepend(card);
    }
</script>
@endauth
<script>
        $(".color-card").click(function(e){
            var color = e.target.className;
            console.log(color);
            $(".bg-color").css("background-color",color);
            $("#color_code").val(color)

        });
        // $(".blue").click(function(){
        //     $(".bg-color").css("background-color","blue");
        //     $("#color_code").val("blue")
        // });

        // $(".red").click(function(){
        //         $(".bg-color").css("background-color","red");
        //         $("#color_code").val("red")
        // });
        // $(".brown").click(function(){
        //     $(".bg-color").css("background-color","brown");
        //     $("#color_code").val("brown")
        // });
        // $(".green").click(function(){
        //     $(".bg-color").css("background-color","green");
        //     $("#color_code").val("green")
        // });
        // $(".pink").click(function(){
        //     $(".bg-color").css("background-color","pink");
        //     $("#color_code").val("pink")
        // });
        // $(".black").click(function(){
        //     $(".bg-color").css("background-color","black");
        //     $("#color_code").val("black")
        // });
        // $(".orange").click(function(){
        //     $(".bg-color").css("background-color","orange");
        //     $("#color_code").val("orange")
        // });
        // $(".purple").click(function(){
        //     $(".bg-color").css("background-color","purple");
        //     $("#color_code").val("purple")
        // });
        // $(".grey").click(function(){
        //     $(".bg-color").css("background-color","grey");
        //     $("#color_code").val("grey")
        // });
</script>
