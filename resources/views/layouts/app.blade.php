<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- Styles -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <title>{{ config('app.name', 'Trello') }}</title>
    <link rel="icon" href="{{ asset('assets/media/icon/fevicon.png') }}" type="image/gif">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body style="background-color:blue ">
    <div id="app" >
        @auth
            @include('layouts.partials._header')
            <div id="create-team" class="d-none">
                <div class="wrapper">
                    <div id="close-create-team">X</div>
                    <div class="content-wrapper row">
                        <div class="create col-md-6">
                            <div class="create-wrapper">
                                <h4 class="dark-text">Lets Build a Team</h4>
                                <h5>Boost your productivity by making it easier for everyone to access boards in one location.</h5>
                                <span class="label dark-text">Team Name</span>
                                <input type="text" name="team_name" id="team-name" class="my-input p-2">
                                <span class="light-text info pt-1">This is the name of your company, team or organization.</span>
                                <span class="label dark-text">Team Name</span>
                                <select name="team_type" id="team_type" class="my-input p-2">
                                    @foreach ($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                <span class="label dark-text">Description <span class="optional-text">Optional</span></span>
                                <textarea name="team_description" id="team-description" cols="30" rows="5" class="my-input p-2"></textarea>
                                <span class="light-text pb-3 pt-1">This is the name of your company, team or organization.</span>
                                <button class="my-submit-btn disabled-btn" id="team-submit" disabled>Continue</button>
                            </div>
                        </div>
                        <div class="image-section col-md-6 text-center">
                            <img src="https://a.trellocdn.com/prgb/dist/images/organization/empty-board.286f8fc83e01c93ed27e.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div id="add-members" class="d-none">
                <div class="wrapper">
                    <div id="close-member-modal" class="close-btn">X</div>
                    <div class="content-wrapper row">
                        <div class="create col-md-6">
                            <div class="create-wrapper">
                                <input type="text" name="team_id" id="add-member-to-team" value="7">
                                <h4 class="dark-text">Invite Your Team</h4>
                                <h5 class="light-text">Trello makes your teamwork best work. Invite your new team members to get going!</h5>

                                <span class="label dark-text">Team Members</span>
                                <input type="text" name="members" id="members" class="my-input p-2">
                                <span class="light-text info pt-1"> <strong>Pro Tip!</strong> Invite all members comma saperated</span>

                                <button class="member-submit-btn disabled-btn" id="member-submit" type="submit" disabled>Continue</button>

                                <div class="inline-block text-center mt-2" id="close-member-modal-btn">I'll do this later</div>
                            </div>
                        </div>
                        <div class="image-section col-md-6 text-center">
                            <img src="https://a.trellocdn.com/prgb/dist/images/organization/empty-board.286f8fc83e01c93ed27e.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        @else
            <nav class="navbar navbar-expand-md navbar-light shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}" style="color: #FFF">
                        <i class="fa fa-trello" aria-hidden="true"></i>{{ config('app.name', 'Trello') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" style="color: #FFF" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" style="color: #FFF" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        @endauth
        <main class="py-4 mt-5">
            @yield('content')
        </main>
    </div>
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
    <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/pages/dashboard.js') }}" type="text/javascript"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $('#board-btn').click(function(){
            $('#boards-search').toggleClass('d-none').toggleClass('d-block');
        });
        $('.close-div').click(function(){
            $('#boards-search').toggleClass('d-block').toggleClass('d-none');
        });

        $('.create-btn').click(function(){
            $('#create-block').toggleClass('d-block').toggleClass('d-none');
        });

        $('.close-create-div').click(function(){
            $('#create-block').toggleClass('d-block').toggleClass('d-none');
        });
    </script>
    @include('layouts.partials._mainScripts')
    @include('layouts.partials._header-scripts');
    @yield('script')
</body>
</html>
