@extends('layouts.dashboard')
@section('style')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<style>

</style>
@endsection
@section('content')
<div>
    <div class="section-type col-md-7">
        <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Recently Viewed</p>
    </div>
    <div class="boards mt-3">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <a href="" class="card" style="background-color:blue">
                        <p>Hello</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-4 section-type col-md-12">
        <p><i class="fa fa-user-o"></i>&nbsp;&nbsp;Personal Boards</p>
    </div>

        <div class="boards mt-3">
            <div class="col-md-12">
                <div class="row">
                    @foreach ($boardsPersonal as $board)

                    <div class="col-md-3">
                        <input type="hidden" name="board_id" id="board_id" value="{{$board->id}}">
                    <a href="{{route('boards.boardIndex',$board->id)}}" class="card {{$board->color}}">
                            <p>{{$board->name}}</p>
                        </a>
                    </div>
                    @endforeach

                    <div class="col-md-3">
                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="card justify-content-center create">
                            <p class="text-center m-0">Create New board</p>
                        </button>

                    </div>

                </div>
            </div>
        </div>

    @for($i=0;$i<count($boardsTeam);$i+=2)

    <div class="mt-4 section-type col-md-12 d-flex justify-content-between">
        <div>
            <p><i class="fa fa-users"></i>&nbsp;&nbsp;{{$boardsTeam[$i]->name}}</p>
        </div>
        <div class=" ">
            <a href="{{route('showTeam',[$boardsTeam[$i],'boards'])}}" class="board-info d-inline-block mr-1"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;Boards</a>
            <a href="{{route('showTeam',[$boardsTeam[$i],'members'])}}" class="board-info d-inline-block mr-1"><i class="fa fa-user"></i>&nbsp;&nbsp;Members({{ ($boardsTeam[$i]->users)->count() }})</a>
            <a href="{{route('showTeam',[$boardsTeam[$i],'settings'])}}" class="board-info d-inline-block mr-1"><i class="fa fa-gear"></i>&nbsp;&nbsp;Settings</a>
        </div>
    </div>

    <div class="boards mt-3">
        <div class="col-md-12">
            <div class="row">
                @for ($j=0;$j<count($boardsTeam[$i+1]);$j++)

                <div class="col-md-3">
                    <a href="{{route('boards.boardIndex',$boardsTeam[$i+1][$j]->id)}}" class="card {{$boardsTeam[$i+1][$j]->color}}">
                        <input type="hidden" id="board_id" name="board_id" value="{{$boardsTeam[$i+1][$j]->id}}">
                        <p>{{$boardsTeam[$i+1][$j]->name}}</p>
                    </a>
                </div>
                @endfor

                <div class="col-md-3">
                <button type="button" data-toggle="modal" data-target="#exampleModal" data-id ="{{$boardsTeam[$i]->id}}" class="card justify-content-center create">
                        <p class="text-center m-0">Create New board</p>
                    </button>

                </div>

            </div>
        </div>
    </div>

    @endfor

</div>


</div>


{{-- <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mt-5 py-5" role="document">
                <div class="modal-content">
                    <form action="{{ route('boards.store') }}" method="POST">
                        @csrf
                        <div>
                    <div class="row">
                        <div class="col-md-6 ml-2 m-0 bg-color" style="background-color: blue">
                            <div class="pt-2 form-group m-0">
                                <div class="d-flex justify-content-between">
                                    <input type="hidden" name="color_code" id="color_code" value="blue">
                                    <input type="text" name="name" id="name" placeholder="Add Board title" class="input bg-none form-control">
                                    <button type="button" class="btn btn-close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="text-white" style="font-size: 16px">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group col-md-6 p-0 m-0 mb-1">
                                <select name="team_id" id="team_id" class="bg-none input form-control">
                                    <option class="text-black" value="personal">Personal</option>

                                    @foreach ($allTeams as $team)
                                        <option class="text-black" value="{{$team->id}}">{{$team->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-0  form-group col-md-6  m-0 mb-1">
                                <select name="visibility" id="visibility" class="bg-none input form-control">
                                    <option class="text-black" value="private"><i class="fa fa-lock"></i>Private</option>
                                    <option class="text-black" value="public"><i class="fa fa-lock"></i>Public</option>
                                </select>
                            </div>
                        </div>
                        <form action="" method="POST">
                            @csrf
                            {{-- <div class="col-md-3 m-0 colors form-group" >
                                <div class="row pl-2">
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div  class="color blue"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="red"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="brown"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="green"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="pink"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="black"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div class="orange"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div  class="purple"></div>
                                    </div>
                                    <div class="color-card col-md-4 p-0 pt-2">
                                        <div  class="grey"></div>
                                    </div>
                                </div>
                            </div> --}}
                        </form>
                    </div>
                <div class="form-group m-0">
                    <button type="submit" class=" btn btn-success form-control" style="width: 100px">Create board</button>
                </div>
            </div>
            </form>

        </div>
    </div>
END MODAL --}}
@endsection
@section('script')
    <script>

    </script>
@endsection
