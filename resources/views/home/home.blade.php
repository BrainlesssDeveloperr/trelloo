@extends('layouts.dashboard')
@section('style')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endsection
@section('content')
<div class="ml-5">
    <div class="row">
        <div class="col-md-6 p-0">
            <div class="bg p-2">
                <div class="pl-1 list">
                    <p>complete this task</p>
                    <div class="d-flex  justify-content-between">
                        <p class="d-inline-block bg-danger mb-2 p-1  text-white"><i class="fa fa-clock-o"></i> 2 Aug</p>
                        <div class="avatar p-1 text-center">
                            <span class="avat p-1">AG</span>
                        </div>
                    </div>
                </div>
                <span class="text-white">TP2: new</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 card-action">
            <div class="d-flex p-3 justify-content-between">
                <p class="d-inline-block"><i class="fa fa-clock-o pr-1"></i>Due 2 Aug at 22:59</p>
                <p class="d-inline-block"><i class="fa fa-ellipsis-h"></i></p>
            </div>
            <div class="row mb-2">
                <div class="col-md-6 "><div class="p-1 status text-center"><i class="fa fa-check"></i> Complete</div></div>
                <div class="col-md-6 "> <div class="p-1 status text-center"><i class="fa fa-close"></i> Dismiss</div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


