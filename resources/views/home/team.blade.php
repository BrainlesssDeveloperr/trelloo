@extends('layouts.app')
@section('content')
<section id="team">
    <div class="team-info">
        <div class="image">
            <i class="fa fa-users" aria-hidden="true"></i>
        </div>
        <div id="other-infos" class="other-info">
            <div class="team-name"><h3>{{$team->name}}</h3></div>
            <span class="team-visibility">
                @if ($team->visibility == "private")
                    <i class="fa fa-lock" aria-hidden="true"></i> Private
                @else
                    <i class="fa fa-unlock" aria-hidden="true"></i> Public
                @endif
            </span>
            <div class="description">{{$team->description}}</div>
            <div class="btn btn-light mt-2" type="button" id="edit-team-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit team Profile</div>
        </div>
        <div id="edit-team" class="d-none">
            <div class="wrapper">
                <form action="{{route('team.update',$team->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <label for="" class="dark-text">Team Name</label>
                    <input type="text" class="my-input" name="name" value="{{$team->name}}">
                    <label for="" class="dark-text pt-2">Team Type</label>
                    <select name="type_id" id="type_id" class="my-input">
                        @foreach ($types as $type)
                            <option value="{{$type->id}}" {{($type->id == $team->type_id) ? 'selected' : ''}}>{{$type->name}}</option>
                        @endforeach
                    </select>
                    <label for="" class="dark-text pt-2">description</label>
                    <textarea name="description" id="team-description" cols="30" rows="5" class="my-input">{{$team->description}}</textarea>
                    <button type="submit" class="btn btn-success">save</button>
                    <div id="cancel-edit-team" class="btn">cancel</div>
                </form>
            </div>
        </div>
    </div>
    <div class="team-setting mt-4">
        <ul class="nav nav-tabs my-nav-tabs justify-content-center" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{($tab=="null")? 'active':(($tab=="boards")?'active':'')}}" id="boards-tab" data-toggle="tab" href="#boards" role="tab" aria-controls="boards" aria-selected="true">Boards</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{($tab=="members")?'active':''}}" id="Members-tab" data-toggle="tab" href="#Members" role="tab" aria-controls="Members" aria-selected="false">Members</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{($tab=="settings")?'active':''}}" id="Settings-tab" data-toggle="tab" href="#Settings" role="tab" aria-controls="Settings" aria-selected="false">Settings</a>
            </li>
        </ul>
        <div class="tab-content team-container mt-4" id="myTabContent">
            <div class="tab-pane fade {{($tab=="null")? 'show active':(($tab=="boards")?'show active':'')}}" id="boards" role="tabpanel" aria-labelledby="boards-tab">
                @foreach ($boards as $board)
            <a href="{{route('boards.boardIndex',$board)}}" >
                        <div class="team-board board mt-2 {{$board->color}}">
                            <h5>{{$board->name}}</h5>
                        </div>
                    </a>
                @endforeach
                <div class="board create-board mt-2 ">
                    <button type="button" data-toggle="modal" data-target="#exampleModal" class="m-0 h-100 card justify-content-center create">
                        <p class="text-center m-0">Create New board</p>
                    </button>

                </div>
            </div>
            <div class="tab-pane fade members {{($tab=="members")?'show active':''}}" id="Members" role="tabpanel" aria-labelledby="Members-tab">
                <h3>Invite Your Team</h3>
                <p>Trello makes teamwork your best work. Invite your new team members to get going!</p>
                <h5>Team Members</h5>

                    <form action="{{route('addMember')}}" method="POST">
                        @csrf
                        <div class="d-flex">
                            <input type="hidden" name="team_id" value="{{$team->id}}" class="d-none">
                            <input type="text" name="email" id="search-members" placeholder="e.g. clarissian@cloud.ci">
                            <button type="submit" id="invite-btn" class="btn btn-primary ml-2 disabled-btn" disabled>Invite</button>
                        </div>
                    </form>

                <p><strong>Pro Tip!</strong> Paste as many emails here as needed.</p>
            </div>
            <div class="tab-pane fade team-settings {{($tab=="settings")?'show active':''}}" id="Settings" role="tabpanel" aria-labelledby="Settings-tab">
                <h6>Team Visibility</h6>
                <div class="visibility justify-content-between">
                    <i class="fa fa-lock" aria-hidden="true"></i> <strong>Private</strong>  - This team is private. It's not indexed or visible to those outside the team.
                    <div class="btn btn-light" id="change-visibility">change</div>
                </div>
            </div>
        </div>
    </div>

    <div id="visibility-option" class="d-none">
        <h5 class="text-center">Select Team Visibility</h5>
        <hr>
        <ul id="visibility-list">
            <li id="private-Visibility-option" class="v-option" data-id="private">
                <div class="d-flex"><i class="fa fa-lock" aria-hidden="true"></i>private
                @if ($team->visibility == "private")
                    <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                </div>
                <span>
                    This team is private.It's not indexed or visible to those outside the team
                </span>
            </li>
            <li id="public-Visibility-option" class="v-option" data-id="public">
                <div class="d-flex"><i class="fa fa-unlock" aria-hidden="true"></i> Public
                @if ($team->visibility == "public")
                    <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                </div>
                <span>
                    This team is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the team can add and edit team boards.
                </span>
            </li>
        </ul>
    </div>
</section>

@endsection
@section('script')
    <script>
        $('body').css('background-color','#fff');
    </script>
    <script>
        $('#edit-team-btn').click(function(){
            $('#other-infos').toggleClass('d-none');
            $('#edit-team').toggleClass('d-block');
        })
        $('#cancel-edit-team').click(function(){
            $('#other-infos').toggleClass('d-none');
            $('#edit-team').toggleClass('d-block');
        })
        $("#search-members").keyup(function(){
            var value = $.trim($("#search-members").val());
            if(value != ""){
                $("#invite-btn").removeAttr("disabled");
                $("#invite-btn").removeClass("disabled-btn");
            }else{
                $("#invite-btn").attr("disabled",true);
                $("#invite-btn").addClass("disabled-btn");
            }
        });
        $("#change-visibility").click(function(){
            $('#visibility-option').toggleClass('d-none');
        });
    </script>
    <script>
        $('#visibility-list').on("click","li",function (e) {

            var newVisibility = $(this).data("id");
            var teamId = "{{$team->id}}";
            var route = "{{ route('ajax.changeVisibility') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'teamId': teamId,
                    'visibility':newVisibility
                },
                dataType: 'json',
                success: function(success){
                    if(success == "success"){
                        if(newVisibility == "private"){
                            $("#private-Visibility-option").html(`
                                <div class="d-flex"><i class="fa fa-lock" aria-hidden="true"></i>private
                                <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <span>
                                    This team is private.It's not indexed or visible to those outside the team
                                </span>
                            `);
                            $('#public-Visibility-option').html(`
                                <div class="d-flex"><i class="fa fa-unlock" aria-hidden="true"></i> Public
                                </div>
                                <span>
                                    This team is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the team can add and edit team boards.
                                </span>
                            `);
                            $('.team-visibility').html(`
                                <i class="fa fa-lock" aria-hidden="true"></i>private
                            `);
                        }else if(newVisibility == "public"){
                            $("#private-Visibility-option").html(`
                                <div class="d-flex"><i class="fa fa-lock" aria-hidden="true"></i>private
                                </div>
                                <span>
                                    This team is private.It's not indexed or visible to those outside the team
                                </span>
                            `);
                            $('#public-Visibility-option').html(`
                                <div class="d-flex"><i class="fa fa-unlock" aria-hidden="true"></i> Public
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <span>
                                    This team is public. It's visible to anyone with the link and will show up in search engines like Google. Only those invited to the team can add and edit team boards.
                                </span>
                            `);
                            $('.team-visibility').html(`
                                <i class="fa fa-unlock" aria-hidden="true"></i>public
                            `);
                        }
                        $('#visibility-option').toggleClass('d-none');
                    }
                }
            });
        });
    </script>
@endsection
