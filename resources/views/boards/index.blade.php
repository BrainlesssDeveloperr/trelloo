@extends('layouts.app')
@section('title','Board')
@section('content')

    <div class="container-fluid" id="board-page-container">
        <div class="board-header">
            <div class="d-flex" style="color: white">
                <h3 class="d-flex align-items-center" style="margin: 0!important; font-weight:500">Board UI</h3>
                <div class="sep"></div>
                <div class="team-visiblity-main">
                    <a href="#" class="transparent-btn team-visiblity-btn d-inline-flex align-items-center"><i class="fa fa-users"></i>Team Visible</a>

                    <div class="team-visiblity d-none" id="team-visiblity">
                        <div class="team-visiblity-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="text-center">Change Visiblity</div>
                                </div>
                                <div class="col-md-2">
                                    <div class="float-right mr-1 close-team-visiblity">X</div>
                                </div>
                            </div>

                        </div>
                        <div class="team-visiblity-body">
                            <ul id="visibility-type">
                                <li  class="private"><i class="fa fa-user"></i>Private</li>
                                <li  class="team"><i class="fa fa-users"></i>Team</li>
                                <li  class="public"><i class="fa fa-user"></i>Public</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="sep"></div>
                <div class="d-main">
                    <a href="#" class="transparent-btn team-invite-btn d-inline-flex align-items-center">Invite</a>
                    <div class="team-invite d-none" id="team-invite">
                        <div class="team-visiblity-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="text-center">Invite To Board</div>
                                </div>
                                <div class="col-md-2">
                                    <div class="float-right mr-1 close-team-invite">X</div>
                                </div>
                            </div>
                        </div>
                        <div class="team-invite-body mt-3">
                            <div id="addMemberToBoard" class=" p-4" style="background-color: #eee ">
                                <form action="" method="POST">
                                    @csrf
                                    <div class="d-flex">
                                        @if($board->team)
                                            <input type="hidden" name="team_id" value="{{$board->team->id}}" class="d-none">
                                        @endif
                                        <input type="text" class="memberBoard" name="email" id="search-members" placeholder="e.g. clarissian@cloud.ci">
                                        <a href="#"  id="invite-btn-board" class="btn btn-primary ml-2" >Invite</a>
                                    </div>
                                </form>
                            </div>
                            {{-- <form action="">
                                <input type="text" class="form-control" placeholder="Enter Email-id">
                                <button class="btn btn-danger mt-2">Send Invite</button>
                            </form> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="edit-card d-none">
            <div class="container-fulid">
                <div class="row">
                    <div class="col-md-9">
                        <div class="edit-card-name d-flex mb-3">
                            <i class="fas fa-laptop"></i>
                            <p class="ml-3" id="edit-card-name"></p>
                        </div>
                        <div class="additional-info">
                            <div id="additional-info-due"></div>
                        </div>
                        <div class="edit-card-des mb-3">
                            <div class="d-flex">
                                <i class="fas fa-align-left"></i>
                                <p class="ml-3">Description</p>
                            </div>
                            <p class="des-text ml-2" style="font-size: 11px">Add description...</p>
                        </div>
                        <div class="edit-card-checklist-main mb-3">
                            <div class="edit-card-checklist">
                                <div class="row">
                                    <div class="col-md-9 d-flex">
                                        <i class="far fa-check-square"></i>
                                        <p class="ml-3">Checklist</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="btn align-item-end btn-danger">Delete</div>
                                    </div>
                                </div>
                            </div>
                            <div class="checklist-progressbar flex-column mb-3">
                                <span class="progressbar-perc">0%</span>
                                <div class="progressbar">
                                    <div class="progressbar-bg"></div>
                                </div>
                            </div>
                            <div class="checklist-wrapper flex-column mb-3">

                            </div>
                            <div class="btn btn-success">Add item</div>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <p class="close-edit-card text-right">X</p>
                        <p>Add To Card</p>
                        <div class="addMember transparent-btn-card bg-my-gray mb-2">
                            <i class="fa fa-user mr-2"></i> Members
                            <div id="addMemberForm" class="d-none p-4" style="background-color: #eee ">
                                <form action="" method="POST">
                                    @csrf
                                    <div class="d-flex">
                                        {{-- <input type="hidden" name="team_id" value="{{$board->team->id}}" class="d-none"> --}}
                                        <input type="hidden" value="" name="card_id" id="card_id">
                                        <input type="text" class="member" name="email" id="search-members" placeholder="e.g. clarissian@cloud.ci">
                                        <a href="#"  id="invite-btn" class="btn btn-primary ml-2" >Invite</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="main-check transparent-btn-card bg-my-gray mb-2">
                            <i class="far fa-check-square mr-2"></i> Checklist
                            <div class="check d-none">
                                <div class="d-flex justify-content-between">
                                    <label for="">Enter Checklist </label>
                                    <div class="check-close">X</div>
                                </div>
                                <input type="text"
                                    class="form-control"
                                    name="checklist" id="checklist" placeholder="Enter">
                                <a href="#" class="btn btn-success mt-2" id="btn_check">Done</a>
                            </div>
                        </div>
                        <div class="main-due transparent-btn-card bg-my-gray">

                            <i class="far fa-clock mr-2"></i>Due Date
                            <div class="due d-none">
                                <div class="d-flex justify-content-between">
                                    <label for="">Enter Due Date </label>
                                    <div class="due-close">X</div>
                                </div>

                                <input type="date"
                                    value="{{old('due_date')}}"
                                    class="form-control"
                                    name="due_date" id="due_date" placeholder="Choose">
                                <a href="#" class="btn btn-success mt-2" id="btn_due">Done</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="board-content d-inline-flex mt-3">

        </div>

    </div>

@endsection
@section('style')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endsection

@section('script')
    <script>
        var color = `{{$board->color}}`
            $("body").css("background-color",color);
            // $("#header").css("background-color",color);

    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.js"></script>
    <script>
        flatpickr("#due_date", {
            enableTime: true,
            minDate: "today",
        });
    </script>
    <script>
        var user = {{auth()->user()->id}}
        var current_card_id = 0;
        var board_id = {{$board->id}}
        var list_id=2;
        var card_id = 1;
        var array = {!!json_encode($arr)!!};
        var checklistArray = {!!json_encode($checklistArr)!!};
        var userArray = {!!json_encode($userArr)!!};
        var teamId =0;

        if('{{$board->type}}'.localeCompare('personal')==0){
            teamId =0;
            alert('{{$board->type}}'.localeCompare('personal'));
        }
        else{
            teamId ='{{$board->team_id}}';
        }
        var totalNo = 0;
        var percPer = 0;
        var width = 0;
        var des_val;
        Echo.private(`board.${teamId}`)
        .listen('BoardEvent', (e) => {
            console.log(e.message);
            if(user!=e.message[4]){
                if(e.message[2]=='card' && e.message[0]==board_id){
                    $('#'+e.message[3]).children('.mycards').append(`
                    <div class="my-card mb-2" id="card_${e.message[1].id}" data-dbid="${e.message[1].id}" data-parent="${e.message[3]}">
                        <div class="my-card-heading">
                            <div class="card-name">${e.message[1].name}</div>
                            <a href="#" class=""><i class="fa fa-pencil"></i></a>

                        </div>
                        <div class="d-flex mt-1 justify-content-between">
                            <div class="d-flex">
                                <div class="oncard-due">
                                </div>
                                <div class="oncard-check">
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="oncard-user">
                                </div>
                            </div>
                        </div>
                    </div>`);
                }else if(e.message[2]=='list' && e.message[0]==board_id){
                    let new_id = (parseInt(e.message[3].split('_')[1]))+1;
                    $('#'+e.message[3]).html(`
                        <div class="my-list-heading mb-2">
                            <h4>${e.message[1].name}</h4>
                            <div class="list-sub-menu">
                                <a href="#"><i class="fas fa-ellipsis-h"></i></a>
                            </div>
                        </div>
                        <div class="mycards sortable" ></div>
                        <a href="#" class="transparent-btn-card add-card d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add a card</a>
                    `).css({
                        'background-color': '#EBECF0',
                        'border-radius': '5px',
                        'padding': '12px'
                    });
                    $('#'+e.message[3]).attr('data-dbId',e.message[1].id);
                    $('.board-content').append(`
                        <div class="my-list" id="list_${new_id}" data-id="list_${new_id}">
                            <a href="#" class="add-list transparent-btn d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add List</a>
                            <div class="mycards sortable" ></div>
                        </div>
                    `);
                }else if(e.message[2]=='card_due' && e.message[0]==board_id){
                    $('#card_'+e.message[1].id+' .oncard-due').html(`${e.message[3]}`);
                }else if(e.message[2]=='card_check' && e.message[0]==board_id){
                    $('#card_'+e.message[1].id+' .oncard-check').html(`${e.message[3]}`);
                }else if(e.message[2]=='card_user' && e.message[0]==board_id){
                    $('#card_'+e.message[1].id+' .oncard-user').append(`${e.message[3]}`);
                }
            }
        });
        if(array.length!=0)
            setBoard(array)
        else{
            $('.board-content').append(`
                <div class="my-list" data-id="list_1" id="list_1">
                    <div class="mycards sortable">

                    </div>
                    <a href="#" class="add-list transparent-btn d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add List</a>

                </div>
            `);
        }
        function setCard(success){
            totalNo = 0;
            percPer = 0;
            width = 0;
            $('#edit-card-name').html(`${success[0].name}`);
            if(success[0].description){
                $('.des-text').html(success[0].description);
            }else{
                $('.des-text').html('Add description...');
            }
            if(success[0].due_date){
                $('#additional-info-due').html(`Due Date : ${success[0].due_date}`);
            }else{
                $('#additional-info-due').html(``);
            }
            $('#due_date').attr('placeholder',success[0].due_date?success[0].due_date:'Choose');
            var str_check = '';
            totalNo = success[1].length;
            if(totalNo!=0)
                percPer = 100/totalNo;

            for(let i = 0;i<success[1].length;i++){

                if(success[1][i].status==1){
                    width+=percPer;
                    str_check += `<div class="d-flex">
                        <input type="checkbox" class="cb mr-2 checklist-box" id='${success[1][i].id}' checked>
                        <p>${success[1][i].name}</p>
                    </div>`;
                }else{
                    str_check += `<div class="d-flex">
                        <input type="checkbox" class="cb mr-2 checklist-box" id='${success[1][i].id}'>
                        <p>${success[1][i].name}</p>
                    </div>`;
                }
            }
            $('.checklist-wrapper').html(str_check);
            $('.progressbar-perc').html(width+'%');
            $('.progressbar-bg').css('width',width+'%');

        }
        function setBoard(array){
            let listId=1;
            let cardId=1;
            // console.log(array);
            for(var i=0;i<array.length;i+=2,listId++){
                $('.board-content').append(`
                    <div class="my-list" id="list_${listId}" data-id="list_${listId}" data-dbid="${array[i].id}">
                        <div class="my-list-heading mb-2">
                            <h4>${array[i].name}</h4>
                            <div class="list-sub-menu">
                                <a href="#"><i class="fas fa-ellipsis-h"></i></a>
                            </div>
                        </div>
                        <div class="mycards sortable" ></div>
                        <a href="#" class="transparent-btn-card add-card d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add a card</a>
                    <div>
                `);

                $('#list_'+listId).css({
                    'background-color': '#EBECF0',
                    'border-radius': '5px',
                    'padding': '12px'
                });
                for(var j=0;j<array[i+1].length;j++,cardId++){
                    // console.log(array[i+1][j]);
                    let date = '';
                    let check = `${checklistArray[array[i+1][j].id]}`;
                    let user = `${userArray[array[i+1][j].id]}`;
                    console.log(user);
                    if(array[i+1][j].due_date){
                        date='<i class="fa fa-clock"></i>';
                    }

                    $('#list_'+listId).children('.mycards').append(`
                        <div class="my-card mb-2" id="card_${array[i+1][j].id}" data-parent="list_${listId}" data-dbid="${array[i+1][j].id}">
                            <div class="my-card-heading">
                                <div class="card-name">${array[i+1][j].name}</div>
                                <a href="#" class=""><i class="fa fa-pencil"></i></a>
                            </div>
                            <div class="d-flex mt-1 justify-content-between">
                                <div class="d-flex">
                                    <div class="oncard-due">
                                        ${date}
                                    </div>
                                    <div class="oncard-check">
                                        ${check}
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="oncard-user">
                                        <img src="${user}" alt="">
                                    </div>
                               </div>
                            </div>
                        </div>
                    `);
                    // for(var k=0;k<user.length;k++){
                    //     $('#list_'+listId).children('.mycards').append(`
                    //         <div class="d-flex">
                    //             <img src="${user[k]}" alt="">
                    //         </div>
                    //     `);
                    // }
                    // $('#list_'+listId).children('.mycards').append(`
                    //         </div>
                    //     </div>
                    // `);
                }
            }
            $('.board-content').append(`
                <div class="my-list" data-id="list_${listId}" id="list_${listId}">
                    <div class="mycards sortable">

                    </div>
                    <a href="#" class="add-list transparent-btn d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add List</a>

                </div>
            `);
            listId++;
            list_id = listId;
            card_id = cardId;
        }
        $('.board-content').on('click','.add-card',function(){
            $(this).parent().append(`<div class="add-card-form">
                                        <form action="">
                                            <textarea class="form-control" placeholder="Enter Card Name"></textarea>
                                            <div class="d-flex add-remove mt-2">
                                                <button class="add-card-btn btn btn-success mr-2">Add Card</button>
                                                <a href="#" class="close-add-card d-flex align-items-center">X</a>
                                            </div>
                                        </form>
                                    </div>`);
            $(this).remove();
        });
        $('.board-content').on('click','.close-add-card',function(){
            $($($($(this).parent()).parent()).parent()).parent().append(
                `<a href="#" class="transparent-btn-card add-card d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add a card</a>`
            );
            $($($(this).parent()).parent()).parent().remove();
        });
        $('.board-content').on('click','.add-list',function(){
            $(this).parent().html(`<div class="add-list-form">
                                        <form action="">
                                            <input type="text" class="form-control" placeholder="Enter List Name">
                                            <div class="d-flex add-remove mt-2">
                                                <button class="add-list-btn btn btn-success mr-2">Add List</button>
                                                <a href="#" class="close-add-list d-flex align-items-center">X</a>
                                            </div>
                                        </form>
                                    </div>`);
        });
        $('.board-content').on('click','.close-add-list',function(){
            $($($($(this).parent()).parent()).parent()).parent().html(
                `<a href="#" class="add-list transparent-btn d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add List</a>`
            );
        });
        $('.board-content').on('click','.add-list-btn',function(){
            let list_name = $($(this).parent()).parent().children('input').val();
            let list = $($($($(this).parent()).parent()).parent()).parent();
            $($($($(this).parent()).parent()).parent()).parent().append(`
                <div class="my-list-heading mb-2">
                    <h4>${list_name}</h4>
                    <div class="list-sub-menu">
                        <a href="#"><i class="fas fa-ellipsis-h"></i></a>
                    </div>
                </div>
                <div class="mycards sortable" ></div>
                <a href="#" class="transparent-btn-card add-card d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add a card</a>
            `);
            $($($($(this).parent()).parent()).parent()).parent().css({
                'background-color': '#EBECF0',
                'border-radius': '5px',
                'padding': '12px'
            });
            $($($(this).parent()).parent()).parent().remove();

            $('.board-content').append(`
                <div class="my-list" id="list_${list_id}" data-id="list_${list_id}">
                    <a href="#" class="add-list transparent-btn d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add List</a>
                    <div class="mycards sortable" ></div>
                </div>
            `);

            let route = "{{ route('ajax.storeList') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'boardId': board_id,
                    'name': list_name,
                    'list_id': list.attr('id')
                },
                dataType: 'json',
                success: function(success){
                    list.attr('data-dbId',success['id']);
                }
            });
            list_id++;
        });
        $('.board-content').on('click','.add-card-btn',function(){
            var parent = $($($($(this).parent()).parent()).parent()).parent().data('id');
            var parent_id = $($($($(this).parent()).parent()).parent()).parent().data('id').split('_')[1];
            var parent_dbid = $($($($(this).parent()).parent()).parent()).parent().data('dbid');
            let card_name = $($(this).parent()).parent().children('textarea').val();
            $($($($($(this).parent()).parent()).parent()).parent()).children('.mycards').append(`
                <div class="my-card mb-2" id="tempcard" data-parent="list_${parent_id}">
                    <div class="my-card-heading">
                        <div class="card-name">${card_name}</div>
                        <a href="#" class=""><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            `);
            let card = $('#tempcard');
            let route = "{{ route('ajax.storeCard') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'listId': parent_dbid,
                    'name': card_name,
                    'parent': parent
                },
                dataType: 'json',
                success: function(success){
                    card.attr('data-dbId',success['id']);
                    card.attr('id','card_'+success['id']);
                }
            });
            card_id++;
            $($($($(this).parent()).parent()).parent()).parent().append(`

                <a href="#" class="transparent-btn-card add-card d-flex align-items-center"><i class="fa" style="font-size: 18px;">+</i>Add a card</a>
            `);
            $($($(this).parent()).parent()).parent().remove();
        });

        $('.des-text').click(function(){
            des_val = $(this).html();
            $(this).parent().append(`<form action="" class="des-form">
                                            <textarea class="form-control" id="des-textarea">${des_val}</textarea>
                                            <div class="d-flex add-remove mt-2">
                                                <button class="add-des-btn btn btn-success mr-2">Submit</button>
                                                <a href="#" class="close-des d-flex align-items-center">X</a>
                                            </div>
                                        </form>`);
            $(this).addClass('d-none');
        });
        $('.edit-card').on('click','.close-des',function(e){

            $('.des-text').html(des_val).removeClass('d-none');
            $($('.des-text').siblings( ".des-form")).remove();
        });
        $('.edit-card').on('click','.add-des-btn',function(e){
            e.preventDefault();
            let route = "{{ route('ajax.updateCardDes') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'card_id': current_card_id,
                    'des':$('#des-textarea').val()
                },
                dataType: 'json',
                success: function(success){
                    $($('.des-text').siblings( ".des-form")).remove();
                    $('.des-text').removeClass('d-none');
                    setCard(success);
                }
            });
        });

        $(".board-content .my-list").hover(function(){
            $(".sortable").sortable({
                revert: true
            });
        });
        $('.board-content').on('click','.my-card',function(e){
            // console.log($('.edit-card'));
            e.stopPropagation();
            current_card_id = $(this).data('dbid');

            $('.edit-card').removeClass('d-none').addClass('d-block');
            let route = "{{ route('ajax.getCard') }}";
            $.ajax({
                url: route,
                method: "GET",
                data:{
                    _token: "{{ csrf_token() }}",
                    'card_id': current_card_id,
                },
                dataType: 'json',
                success: function(success){
                    setCard(success);
                }
            });
        });
        $('.due-close').click(function(e){
            e.stopPropagation();
            $('.due').removeClass('d-block').addClass('d-none');
        });
        $('.check-close').click(function(e){
            e.stopPropagation();
            $('.check').removeClass('d-block').addClass('d-none');
        });
        $('.close-edit-card').click(function(){
            $('.edit-card').removeClass('d-block').addClass('d-none');
        });
        $('.team-visiblity-btn').click(function(e){
            e.stopPropagation();
            $('.team-visiblity').removeClass('d-none').addClass('d-block');
            $('.team-invite').removeClass('d-block').addClass('d-none');
        });
        $('.close-team-visiblity').click(function(){
            $('.team-visiblity').removeClass('d-block').addClass('d-none');
        });
        $('.team-invite-btn').click(function(e){
            e.stopPropagation();
            $('.team-invite').removeClass('d-none').addClass('d-block');
            $('.team-visiblity').removeClass('d-block').addClass('d-none');
        });
        $('.close-team-invite').click(function(){
            $('.team-invite').removeClass('d-block').addClass('d-none');
        });
        $('.team-invite').click(function(e){
            e.stopPropagation();
        });
        $('.team-visiblity').click(function(e){
            e.stopPropagation();
        });
        $('.checklist-wrapper').on('click','.checklist-box',function(e){
            let id = $(this).attr('id');
            if ($(this).prop('checked')==true){
                width += percPer;

                $('.progressbar-perc').html(width+'%');
                $('.progressbar-bg').css('width',width+'%');
                let route = "{{ route('ajax.updateChecklist') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        'id': id,
                        'checked': 1
                    },
                    dataType: 'json',
                    success: function(success){
                        $('#card_'+current_card_id+' .oncard-check').html(`
                            <i class="far fa-check-square mr-1"></i>${success[2]}
                        `);
                        setCard(success);
                    }
                });

            }else{
                width -= percPer;
                $('.progressbar-perc').html(width+'%');
                $('.progressbar-bg').css('width',width+'%');
                let route = "{{ route('ajax.updateChecklist') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        'id': id,
                        'checked': 0
                    },
                    dataType: 'json',
                    success: function(success){
                        $('#card_'+current_card_id+' .oncard-check').html(`
                            <i class="far fa-check-square ml-2 mr-1"></i>${success[2]}
                        `);
                        setCard(success);
                    }
                });
            }

        });
        $('#invite-btn-board').click(function(e){
            $('.team-invite').removeClass('d-block').addClass('d-none');
        })
        $(document).click(function (e) {

            if (!$(e.target).is('#team-invite')) {
                $('.team-invite').removeClass('d-block').addClass('d-none');
            }
            if(!$(e.target).is('#team-visiblity')){
                $('.team-visiblity').removeClass('d-block').addClass('d-none');
            }


        });
        $('.main-due').click(function(){
            $('.due').removeClass('d-none').addClass('d-block');
        });
        $('.main-check').click(function(){
            $('.check').removeClass('d-none').addClass('d-block');
        });
        $('#btn_check').click(function(){
            let checklist = $('#checklist').val();
            $('#checklist').val("");
            let route = "{{ route('ajax.insertChecklist') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'card_id': current_card_id,
                    'checklist': checklist
                },
                dataType: 'json',
                success: function(success){
                    $('#card_'+current_card_id+' .oncard-check').html(`
                        <i class="far fa-check-square mr-1 ml-2"></i>${success[2]}
                    `);
                    setCard(success);
                }
            });
        });

        $('.close-edit-card').click(function(){
            $('.edit-card').removeClass('d-block').addClass('d-none');
        });
        $('.addMember').click(function(){
            $('#addMemberForm').removeClass('d-none').addClass('d-inline-block');
        });
        $("#invite-btn").click(function(){
            let route = "{{ route('ajax.addMemberToCard') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'member': $('.member').val(),
                    'card_id': current_card_id
                },
                dataType: 'json',
                success: function(success){
                    $('#addMemberForm').removeClass('d-none').addClass('d-inline-block');
                    $('#card_'+current_card_id+' .oncard-user').append(`
                        <img src="${success[0]}" alt="">
                    `);
                }
            });
        });
        $('#btn_due').click(function(){
            let date = $('#due_date').val();
            let route = "{{ route('ajax.updateCardDue') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'card_id': current_card_id,
                    'due_date': date
                },
                dataType: 'json',
                success: function(success){
                    $('#card_'+current_card_id+' .oncard-due').html(`
                        <i class="fa fa-clock mr-2"></i>
                    `)
                    setCard(success);
                }
            });
        });

        $("#invite-btn-board").click(function(){
            let route = "{{ route('ajax.addMemberToBoard') }}";
            var value = $('#search-members').val();
            $('#search-members').val("");
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'board_id': board_id,
                    'memberBoard': value,
                },
                dataType: 'json',
                success: function(success){

                }
            });
        });

        $("#visibility-type").click(function(e){
            let route = "{{ route('ajax.changeVisibility') }}";
            var visibility = e.target.className;
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'board_id': board_id,
                    'visibility':visibility
                },
                dataType: 'json',
                success: function(success){
                    $('#addMemberForm').removeClass('d-none').addClass('d-inline-block');
                }
            });
        });
        $("#invite-btn-board").click(function(){
            let route = "{{ route('ajax.addMemberToBoard') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'board_id': board_id,
                    'memberBoard': $('.memberBoard').val(),
                },
                dataType: 'json',
                success: function(success){

                }
            });
        });

        $("#visibility-type").click(function(){
            let route = "{{ route('ajax.changeVisibility') }}";
            var visibility = $(this).attr('class');
            alert(visibility);
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'board_id': board_id,
                    'visibility':visibility
                },
                dataType: 'json',
                success: function(success){

                }
            });
        });
    </script>
@endsection
