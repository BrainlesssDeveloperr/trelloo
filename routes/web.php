<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){
    //home page
    Route::get('/dashboard/home','HomeController@dashboardIndex');
    Route::get('/dashboard/home','HomeController@index');

    //boards route ::
    //view specific board
    Route::get('/board/{board}','BoardsController@boardIndex')->name('boards.boardIndex')->middleware('hasBoard');
    //view all boards
    Route::resource('/dashboard/boards','BoardsController');

    Route::get('teams/{id}/{tab}','TeamsController@showTeam')->name('showTeam')->middleware('hasTeam');

    // view teams
    Route::get('/dashboard/team', function(){
        return view('home.team');
    });
    //show team
    Route::get('teams/{id}','TeamsController@showTeam')->name('showTeam');

    Route::Resource('team','TeamsController');
    Route::post('teams/addMember','TeamsController@addMember')->name('addMember');

    //ajax routes on board
    Route::post('/board/list/create','BoardsController@createList')->name('ajax.storeList');
    Route::post('/board/list/card/updatedue','BoardsController@updateDue')->name('ajax.updateCardDue');
    Route::post('/board/list/card/updatedes','BoardsController@updateDes')->name('ajax.updateCardDes');
    Route::get('/board/list/card/get','BoardsController@getCard')->name('ajax.getCard');
    Route::post('/board/list/card/checklist/insert','ChecklistsController@insertChecklist')->name('ajax.insertChecklist');
    Route::post('/board/list/card/checklist/update','ChecklistsController@updateChecklist')->name('ajax.updateChecklist');
    Route::post('/board/list/create','BoardsController@createList')->name('ajax.storeList');
    Route::post('/board/list/card/create','CardsController@createCard')->name('ajax.storeCard');
    Route::post('/board/list/card/create/member','CardsController@addMemberToCard')->name('ajax.addMemberToCard');
    Route::post('/board/invite/member','BoardsController@addMemberToBoard')->name('ajax.addMemberToBoard');
    Route::post('/board/changeVisibility','BoardsController@changeVisibility')->name('ajax.changeVisibility');

    //ajax route on team
    Route::post('teams/changeVisibility','TeamsController@changeVisibility')->name('ajax.changeVisibility');
    Route::post('teams/addMembers','TeamsController@addMembers')->name('ajax.addMembers');
    Route::post('teams/storeMyTeam','TeamsController@storeMyTeam')->name('ajax.storeMyTeam');

});
