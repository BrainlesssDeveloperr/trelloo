<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team_Type extends Model
{
    protected $table="team_types";
    protected  $primaryKey = 'name';
    public $incrementing = false;
}
