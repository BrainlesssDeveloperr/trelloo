<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $guarded = [];
    public function list(){
        return $this->belongsTo(Lists::class);
    }
    public function checklists(){
        return $this->hasMany(Checklist::class);
    }
    public function notifications(){
        return $this->belongsToMany(notification::class);
    }
    public function users(){
        return $this->belongsToMany(User::class);
    }
}
