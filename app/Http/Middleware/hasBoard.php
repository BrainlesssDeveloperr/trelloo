<?php

namespace App\Http\Middleware;

use Closure;

class hasBoard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if((auth()->user()->boards()->where('board_id',$request->route('board')->id)->get())->count() == 0){
                return redirect(abort(401));
            }
            return $next($request);
    }
}
