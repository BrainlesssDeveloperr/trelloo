<?php

namespace App\Http\Middleware;

use Closure;

class hasTeam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((auth()->user()->teams()->where('team_id',$request->route('id'))->get())->count() == 0){
            return redirect(abort(401));
        }
        return $next($request);
    }
}
