<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;
use App\Lists;
use App\Card;
use App\Events\BoardEvent;
use App\Events\NotificationEvent;
use App\message;
use App\notification;
use App\Team;
use App\Team_Type;
use App\User;
use Illuminate\Support\Facades\Auth;


class BoardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $t = [];
        $t[] = ['type','=','personal'];
        $t[] = ['user_id','=', Auth::id()];
        // dd($t);
        $boardsPersonal = Board::where($t)->get();

        $teams = auth()->user()->teams;
        // dd($teams);
        $boardsTeam = [];
        foreach($teams as $team){
            $boardsTeam[] = $team;
            $boardsTeam[] = $team->boards;
        }
        $types = Team_Type::all();
        $allTeams = Team::all();
        // dd($allTeams);
        $notifications = notification::where('to_user','=',auth()->user()->id)->get();
        $personalBoards = Board::where(['type'=>'personal','user_id'=>auth()->id()])->get()->take(5);
        return view('home.board',compact([
            'boardsPersonal','boardsTeam','types','allTeams','notifications','personalBoards','teams'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $personal = "personal";
        $team = "team";
        $data = [
            'name'=>$request->name,
            'visibility'=>$request->visibility,
            'color'=>$request->color_code
        ];
        $type = $request->team_id == 0 ? $personal : $team;
        $data += [
            'type'=>$type,
            'user_id'=>Auth::id()
        ];
        // dd($type);
        if($type==$team){
            $t= Team::find($request->team_id);
            $board = $t->boards()->create($data);
            // dd($board);
        }
        else{
            $board = Board::create($data);
        }
        $user =  Auth::user();
        $user->boards()->attach($board->id,['isAdmin'=>1]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board)
    {
        //
    }
    public function boardIndex(Board $board){
        $arr = [];
        $checklistArr = [];
        $userArr = [];
        $lists = $board->lists;
        foreach($lists as $list){
            $arr[] = $list;
            $arr[] = $list->cards;
            foreach($list->cards as $card){
                $str = '';
                $userStr = [];
                if($card->users->count()>0){
                    foreach($card->users as $user){
                        $userStr[] = $user->avatar;
                    }
                }
                if($card->checklists->count()>0){
                    $str .= $card->checklists->count();
                    $count = 0;
                    foreach($card->checklists as $check){
                        if($check->status == 1)
                            $count+=1;
                    }
                    $str = '<i class="far fa-check-square ml-2 mr-1"></i>'.$count.'/'.$str;
                }
                $userArr[$card->id] = $userStr;
                $checklistArr[$card->id] = $str;
            }
        }
        $types = Team_Type::all();
        $notifications = notification::where('to_user','=',auth()->user()->id)->get();
        $personalBoards = Board::where(['type'=>'personal','user_id'=>auth()->id()])->get()->take(5);
        $teams = Team::all();
        return view('boards.index',compact(['arr','types','notifications','board','checklistArr','userArr','personalBoards','teams']));
    }

    public function createList(Request $request){
        $board = Board::find($request->boardId);
        $list = $board->lists()->create([
            'name'=>$request->name,
        ]);
        $arr = [];
        $arr[] = $board->id;
        $arr[] = $list;
        $arr[] = 'list';
        $arr[] = $request->list_id;
        $arr[] = auth()->user()->id;
        if($board->team)
            $arr[] = $board->team->id;
        else
            $arr[] = 0;
        event(new BoardEvent($arr));
        return json_encode($list);
    }


    public function updateDue(Request $request){
        $card = Card::find($request->card_id);
        $card->update([
            'due_date'=>$request->due_date
        ]);


        $users = $card->users;
        if($users){
            foreach($users as $user){
                $notification = notification::where([['card_id','=',$request->card_id],['to_user','=',$user->id]])->first();
                $sender = auth()->user();
                $card = Card::find($request->card_id);
                $list = $card->list;
                $board = $list->board;
                $message = message::where('type','=','due-date')->firstOrFail();
                $notification->messages()->attach($message->id);

                $date = date('d-m', strtotime($request->due_date));

                $arrr = [];
                $arrr[] = $user->id;
                $arrr[] = $message->message;
                $arrr[] = $message->type;
                $arrr[] = $card;
                $arrr[] = $list;
                $arrr[] = $board;
                $arrr[] = $sender;
                $arrr[] = $date;

                event(new NotificationEvent($arrr));
            }
        }

        $checklists = $card->checklists;
        $arr = [];
        $arr[] = $card;
        $arr[] = $checklists;
        $newArr = [];
        $newArr[] = $card->list->board->id;
        $newArr[] = $card;
        $newArr[] = 'card_due';
        $newArr[] = '<i class="fa fa-clock"></i>';
        $newArr[] = auth()->user()->id;
        if($card->list->board->team)
            $newArr[] = $card->list->board->team->id;
        else
            $newArr[] = 0;
        event(new BoardEvent($newArr));
        return json_encode($arr);
    }
    public function updateDes(Request $request){
        $card = Card::find($request->card_id);
        $card->update([
            'description'=>$request->des
        ]);
        $checklists = $card->checklists;
        $arr = [];
        $arr[] = $card;
        $arr[] = $checklists;
        return json_encode($arr);
    }
    public function getCard(Request $request){
        $card = Card::find($request->card_id);
        $checklists = $card->checklists;
        $arr = [];
        $arr[] = $card;
        $arr[] = $checklists;
        return json_encode($arr);
    }
    public function addMemberToBoard(Request $request){
        $user = User::where('email',$request->memberBoard)->first();
        $arr = [];
        $arr[] = $user->id;
        $arr[] = "you have been added to a board";
        event(new NotificationEvent($arr));
        $user->boards()->attach($request->board_id,['isAdmin'=>0]);
    }
    public function changeVisibility(Request $request){
        // dd($request);
        $board= Board::where('id',$request->board_id)->first();
        $board->update([
            'visibility'=>$request->visibility
        ]);
    }
}
