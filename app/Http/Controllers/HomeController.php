<?php

namespace App\Http\Controllers;

use App\Board;
use App\notification;
use App\Team;
use App\Team_Type;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $types = Team_Type::all();
        $notifications = notification::where('to_user','=',auth()->user()->id)->get();
        $teams = auth()->user()->teams;
        $personalBoards = Board::where(['type'=>'personal','user_id'=>auth()->id()])->get()->take(5);
        return view('home.home',compact(['types','notifications','teams','personalBoards']));
    }
}
