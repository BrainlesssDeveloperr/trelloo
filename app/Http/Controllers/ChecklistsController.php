<?php

namespace App\Http\Controllers;

use App\Card;
use App\Checklist;
use App\Events\BoardEvent;
use App\Events\NotificationEvent;
use App\message;
use App\notification;
use Illuminate\Http\Request;

class ChecklistsController extends Controller
{
    public function insertChecklist(Request $request){
        $card = Card::find($request->card_id);
        $check = $card->checklists()->create([
            'name'=>$request->checklist
        ]);
        $users = $card->users;
        if($users){
            foreach($users as $user){
                $notification = notification::where([['card_id','=',$request->card_id],['to_user','=',$user->id]])->first();
                $sender = auth()->user();
                $card = Card::find($request->card_id);
                $list = $card->list;
                $board = $list->board;
                $message = message::where('type','=','add-checklist')->firstOrFail();
                $notification->messages()->attach($message->id);
                $arrr = [];
                $arrr[] = $user->id;
                $arrr[] = $message->message;
                $arrr[] = $message->type;
                $arrr[] = $card;
                $arrr[] = $list;
                $arrr[] = $board;
                $arrr[] = $sender;

                event(new NotificationEvent($arrr));
            }
        }
        $arr = [];
        $arr[] = $card;
        $arr[] = $card->checklists;
        $arr[] = $card->checklists()->where('status',1)->get()->count().'/'.$card->checklists->count();
        $newArr = [];
        $newArr[] = $card->list->board->id;
        $newArr[] = $card;
        $newArr[] = 'card_check';
        $newArr[] = '<i class="far fa-check-square ml-2 mr-1"></i>'.$card->checklists()->where('status',1)->get()->count().'/'.$card->checklists->count();
        $newArr[] = auth()->user()->id;
        if($card->list->board->team)
            $newArr[] = $card->list->board->team->id;
        else
            $newArr[] = 0;
        event(new BoardEvent($newArr));
        return json_encode($arr);
    }
    public function updateChecklist(Request $request){
        $checklist = Checklist::find($request->id);
        if($request->checked)
            $checklist->update(['status'=>1]);
        else
            $checklist->update(['status'=>0]);
        $card = $checklist->card;
        $arr = [];
        $arr[] = $checklist->card;
        $arr[] = $checklist->card->checklists;
        $arr[] = $checklist->card->checklists()->where('status',1)->get()->count().'/'.$checklist->card->checklists->count();
        $newArr = [];
        $newArr[] = $card->list->board->id;
        $newArr[] = $card;
        $newArr[] = 'card_check';
        $newArr[] = '<i class="far fa-check-square ml-2 mr-1"></i>'.$card->checklists()->where('status',1)->get()->count().'/'.$card->checklists->count();
        $newArr[] = auth()->user()->id;
        if($card->list->board->team)
            $newArr[] = $card->list->board->team->id;
        else
            $newArr[] = 0;
        event(new BoardEvent($newArr));
        return json_encode($arr);
    }
}
