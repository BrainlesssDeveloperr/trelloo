<?php

namespace App\Http\Controllers;

use App\Card;
use App\Events\BoardEvent;
use App\Events\NotificationEvent;
use App\Lists;
use App\message;
use App\notification;
use App\User;
use Illuminate\Http\Request;
use PDO;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        //
    }
    public function addMemberToCard(Request $request){
        $user = User::where('email',$request->member)->first();
        $rtnArr = [];
        $rtnArr[] = $user->avatar;
        if(!empty($user)){
            $notification = notification::where([['card_id','=',$request->card_id],['to_user','=',$user->id]])->first();
            if(empty($notification)){
                $user->cards()->attach($request->card_id);
                $sender = auth()->user();
                $card = Card::find($request->card_id);
                $list = $card->list;
                $board = $list->board;
                $usersURL = $this->getUserURL($card->users);
                $message = message::where('type','=','add-user')->firstOrFail();
                $notification = notification::create([
                    'from_user' => $sender->id,
                    'to_user' => $user->id,
                    'card_id' => $card->id
                ]);
                $notification->messages()->attach($message->id);

                $arr = [];
                $arr[] = $user->id;
                $arr[] = $message->message;
                $arr[] = $message->type;
                $arr[] = $card;
                $arr[] = $list;
                $arr[] = $board;
                $arr[] = $sender;
                $arr[] = $usersURL;
                $arr[] = auth()->user()->avatar;
                event(new NotificationEvent($arr));
                $user->cards()->attach($request->card_id);
                $newArr = [];
                $newArr[] = $card->list->board->id;
                $newArr[] = $card;
                $newArr[] = 'card_user';
                $newArr[] = '<img src="'.$rtnArr[0].'" alt="">';
                $newArr[] = auth()->user()->id;
                if($card->list->board->team)
                    $newArr[] = $card->list->board->team->id;
                else
                    $newArr[] = 0;
                event(new BoardEvent($newArr));
                event(new NotificationEvent($arr));
                return json_encode($rtnArr);
            }
        }
    }
    public function getUserURL($users){
        $arr = [];
        foreach($users as $user){
            $arr[] = $user->avatar;
        }
        return $arr;
    }
    public function createCard(Request $request){
        // $board = Board::find(1);
        $list = Lists::find($request->listId);
        $card = $list->cards()->create([
            'name'=>$request->name
        ]);

        $arr = [];
        $arr[] = $list->board->id;
        $arr[] = $card;
        $arr[] = 'card';
        $arr[] = $request->parent;
        $arr[] = auth()->user()->id;
        if($list->board->team)
            $arr[] = $list->board->team->id;
        else
            $arr[] = 0;
        event(new BoardEvent($arr));
        return json_encode($card);
    }
}
