<?php

namespace App\Http\Controllers;

use App\Board;
use App\Events\NotificationEvent;
use App\notification;
use App\Team;
use App\Team_Type;
use App\User;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = Team_type::find($request->team_type);
        $team = Team::create([
            'name'=>$request->team_name,
            'type_id'=>$type->id,
            'description'=>$request->team_description,
        ]);
        $team->users()->attach(auth()->user()->id,['isAdmin'=>1]);

        return json_encode($team->id);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        // dd($team);
        $data = $request->only(['name','type_id','description']);
        // dd($data);
        $team->update($data);
        // dd($team->name);
        return ($this->showTeam($team->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        //
    }
    public function showTeam($team,$tab)
    {
        $team = Team::find($team);
        $types = Team_Type::all();
        $notifications = notification::where('to_user','=',auth()->user()->id)->get();
        $boards = $team->boards;
        $personalBoards = Board::where(['type'=>'personal','user_id'=>auth()->id()])->get()->take(5);
        $teams = Team::all();
        return view('home.team',compact(['team','types','notifications','boards','tab','personalBoards','teams']));
    }
    public function addMember(Request $request){
        $user = User::where("email",$request->email)->first();
        if(!empty($user)){
            $team = Team::find($request->team_id);
            if(!$team->isMember($user->id)){
                return redirect()->back();
            }
            $team->users()->attach($user->id,['isAdmin'=>0]);
        }
        return redirect()->back();
    }

    public function changeVisibility(Request $request){
        $team = Team::find($request->teamId);
        $team->update([
            'visibility'=>$request->visibility,
        ]);
        return json_encode("success");
    }

    public function addMembers(Request $request){
        $users = explode(',',$request->members);
        $team = Team::find($request->team_id);
        $arr = [];
        foreach($users as $user){
            $verify_user = User::where("email",$user)->get();
            if(!empty($verify_user[0])){
                if($team->isMember($verify_user[0]->id)){
                    $team->users()->attach($verify_user[0]->id,['isAdmin'=>0]);
                    $arr[] = $verify_user[0]->id;
                    $arr[] = "added in team";
                    event(new NotificationEvent($arr));
                }else{
                    continue;
                }
            }
        }
        return json_encode("success");
    }
}
