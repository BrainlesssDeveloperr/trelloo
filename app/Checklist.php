<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $guarded = [];
    protected $table = 'checklist';
    public function card(){
        return $this->belongsTo(Card::class);
    }
}
