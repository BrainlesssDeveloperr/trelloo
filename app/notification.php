<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
    protected $guarded = [];
    public function messages(){
        return $this->belongsToMany(message::class);
    }
    public function fromUser(){
        return $this->belongsTo(User::class,'from_user');
    }
    public function card(){
        return $this->belongsTo(Card::class);
    }
}
