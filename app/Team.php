<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $guarded= [];


    public function users(){
        return $this->belongsToMany(User::class);
    }
    public function boards(){
        return $this->hasMany(Board::class);
    }
    public function isMember($user_id){
        return empty($this->users()->where('users.id','=',$user_id)->get()[0]);
    }
}
