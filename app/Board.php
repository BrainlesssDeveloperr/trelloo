<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $guarded = [];
    public function users(){
        return $this->hasMany(User::class)->withPivot('isAdmin');
    }
    public function lists(){
        return $this->hasMany(Lists::class);
    }

    public function team(){
        return $this->belongsTo(Team::class);
    }

}
