<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public function getAvatarAttribute(){
        $size = 30;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function teams(){
        return $this->belongsToMany(Team::class);
    }
    public function cards(){
        return $this->belongsToMany(Card::class);
    }
    public function boards(){
        return $this->belongsToMany(Board::class)->withPivot('isAdmin');
    }
    public function notifications(){
        return $this->belongsToMany(notifications::class);
    }
}
